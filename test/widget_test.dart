import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:miles/main.dart';



void main() {

	testWidgets('List update smoke test', (WidgetTester tester) async {
		final String _flutter = 'flutter';

		await tester.pumpWidget(Journeys());

		expect(find.text(_flutter), findsNothing);

		await ADD_ITEM_TO_LIST(tester, _flutter);

		expect(find.text(_flutter), findsOneWidget);
	});
}



Future ADD_ITEM_TO_LIST(WidgetTester tester, String ITEM) async {
	await tester.tap(find.byIcon(Icons.add));
	await tester.pump();
	await tester.enterText(find.byType(EditableText), ITEM);
	await tester.pump();
	await tester.tap(find.text('Add'));
	await tester.pump();
}

