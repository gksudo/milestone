import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(Journeys());

class Journeys extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: 'Milestone',
      home: HomePage(title: 'undertakings'),
    );
  }
}

class HomePage extends StatefulWidget {
  final String title;
  HomePage({Key key, this.title}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _biggerFont = const TextStyle(fontSize: 18.0);
  TextEditingController itemTextController = new TextEditingController();
  List<String> listItems = ['Tekken', 'Music', 'Cooking'];

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Stack(children: <Widget>[
        _items(widget.title),
        _newItemButton(),
      ]),
    );
  }

  Widget _items(String title) {
    return CustomScrollView(
      semanticChildCount: listItems.length,
      slivers: <Widget>[
        CupertinoSliverNavigationBar(
          largeTitle: Text(title),
        ),
        SliverSafeArea(
          top: false,
          minimum: const EdgeInsets.only(top: 8),
          sliver: SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, i) {
                if (i < listItems.length) {
                  return Container(
                    height: 75,
                    child: Center(
                        child: Text(
                      '${listItems[i]}',
                      style: _biggerFont,
                    )),
                  );
                }
                return null;
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget _newItemButton() {
    return Center(
      child: Container(
        alignment: Alignment(0.8, 0.8),
        child: CupertinoButton(
          onPressed: () {
            showCupertinoDialog(
                context: context,
                builder: (BuildContext context) {
                  return _newItemForm();
                });
          },
          child: Icon(Icons.add, size: 50),
        ),
      ),
    );
  }

  Widget _newItemForm() {
    return CupertinoAlertDialog(
        title: new Text('What are you working on?'),
        content: new CupertinoTextField(
          controller: itemTextController,
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
              width: 0.2,
            ),
          ),
        ),
        actions: <Widget>[
          new CupertinoDialogAction(
            child: new Text('Cancel'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          new CupertinoDialogAction(
            child:
                new Text('Add', style: TextStyle(fontWeight: FontWeight.bold)),
            onPressed: () {
              setState(() {
                listItems.add(itemTextController.text);
                itemTextController.clear();
              });
              Navigator.of(context).pop();
            },
          ),
        ]);
  }
}
